const { memeAsync } = require('memejs');
const http = require('http').createServer(async function(req, res) {

    // This will allow requests from anywhere.
    // res.setHeader('Access-Control-Allow-Origin', '*');

    // Only allow requests from localhost:5500 
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:5500');

    // Only allow GET and OPTIONS request methods.
    res.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
    // Allow Content-Type headers
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    // Set the response MIME Type to JSON.
    res.setHeader('Content-Type', 'application/json');

    // Parse the Request URL to a URL object.
    const url = new URL(req.url, `http://${req.headers.host}`);
    // Get the Query Parameters from the URL
    const { searchParams } = url;
    
    if (url.pathname === '/') {
        try {
            const subReddit = searchParams.get('r');
            const meme = await memeAsync( subReddit );
            res.write( JSON.stringify( meme ) );
            res.end();
        } catch (e) {
            console.error( e );
            res.write( JSON.stringify( e ) );
            res.end();
        }
    } else {
        res.statusCode = 400;
        res.end('Illegal Path');
    }
});


http.listen(3000);